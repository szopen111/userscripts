// ==UserScript==
// @name         EdX - Remove play button & fix space-bar
// @namespace    http://damian.sepczuk.com/
// @version      0.1
// @description  EdX - Remove play btn. It covers up the video!
// @author       Damian Sepczuk
// @match        https://courses.edx.org/courses/*/courseware/*
// @grant        GM_addStyle
// ==/UserScript==

(function() {
    'use strict';
    GM_addStyle('.fa-youtube-play { display: none !important; }');
    (function($){

     /**
	 * Copyright 2012, Digital Fusion
	 * Licensed under the MIT license.
	 * http://teamdf.com/jquery-plugins/license/
	 *
	 * @author Sam Sehnert
	 * @desc A small plugin that checks whether elements are within
	 *		 the user visible viewport of a web browser.
	 *		 only accounts for vertical position, not horizontal.
	 */
        $.fn.visible = function(partial){

            var $t				= $(this),
                $w				= $(window),
                viewTop			= $w.scrollTop(),
                viewBottom		= viewTop + $w.height(),
                _top			= $t.offset().top,
                _bottom			= _top + $t.height(),
                compareTop		= partial === true ? _bottom : _top,
                compareBottom	= partial === true ? _top : _bottom;

            return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
        };

    })(jQuery);

    // Adapted from https://greasyfork.org/pl/scripts/25035-disable-space-bar-scrolling/code
    var killSpaceBar = function(evt) {

        var target = evt.target || {},
            isInput = ("INPUT" == target.tagName || "TEXTAREA" == target.tagName || "SELECT" == target.tagName || "EMBED" == target.tagName);

        // if we’re an input or not a real target exit
        if(isInput || !target.tagName) return;

        // if we’re a fake input like the comments exit
        if(target && target.getAttribute && target.getAttribute('role') === 'textbox') return;

        // ignore the space and send a ‘k’ to pause
        if (evt.keyCode === 32) {
            evt.preventDefault();
            $('.vcr button').each(function() {
                if ($(this).visible(true)) {
                    $(this).click();
                    return false;
                }
            });
        }
    };

    document.addEventListener("keydown", killSpaceBar, false);
})();